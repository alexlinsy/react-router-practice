import React, { Component } from 'react'
import { connect } from 'react-redux'

class Post extends Component {
  handleClick = () => {
    this.props.deletePost(this.props.posts.id);
    this.props.history.push('/');
  }
  render() {  
    console.log(this.props);
    const post = this.props.posts ? (
      <div className="post">
        <h4 className="center">{this.props.posts.title}</h4>
        <p>{this.props.posts.body}</p>
        <div className="center">
          <button className="btn grey" onClick={this.handleClick}>
            Delete Post
          </button>
        </div>
      </div>
    ) : (
      <div className="center">Loading post...</div>
    ) 
    return (
      <div className="container">
        { post }
      </div>
    );
  }
  
}

const mapStateToProps = (state, ownProps) => {
  let id = ownProps.match.params.post_id;
  return {
    posts: state.posts.find( (post) => {
      return post.id === id; 
    })
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    deletePost: (id) => { dispatch({type: 'DELETE_POST', id: id}) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Post)
